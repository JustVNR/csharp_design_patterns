﻿namespace Patterns_Structure.PROXY
{
    public class Internet : IInternet
    {
        public void ConnectTo(string host)
        {
            Console.WriteLine($"Connectred to {host}");
        }
    }
}
