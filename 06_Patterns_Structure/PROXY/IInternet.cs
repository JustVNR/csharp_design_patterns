﻿namespace Patterns_Structure.PROXY
{
    public interface IInternet
    {
        void ConnectTo(string host);
    }
}
