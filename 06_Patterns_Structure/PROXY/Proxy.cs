﻿namespace Patterns_Structure.PROXY
{
    public class Proxy : IInternet
    {
        public List<string> BannedSites { get; set; } = new();

        private readonly Internet Internet = new ();

        public Proxy(params string[] bannedSites)
        {
            BannedSites.AddRange(bannedSites);
        }

        public void ConnectTo(string host)
        {

            if (BannedSites.Contains(host))
            {
                Console.WriteLine("Access Denied");
                return;
            }
            Internet.ConnectTo(host);
        }
    }
}
