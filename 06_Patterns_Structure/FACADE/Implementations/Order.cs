﻿using Patterns_Structure.FACADE.Interfaces;

namespace Patterns_Structure.FACADE.Implementations
{
    public class Order : IOrder
    {
        public int PlaceOrderDetails(int cartID, int shippingAddressID)
        {
            Console.WriteLine($"Order placed with cart ID = {cartID} and shipping Address Id = {shippingAddressID}");

            return 1;
        }
    }
}
