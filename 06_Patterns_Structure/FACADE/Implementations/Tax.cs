﻿using Patterns_Structure.FACADE.Interfaces;

namespace Patterns_Structure.FACADE.Implementations
{
    public class Tax : ITax
    {
        public void ApplyTax(int cartID, double taxPercent)
        {
            Console.WriteLine($"{taxPercent}% tax applied on cart with ID {cartID} ");
        }

        public double GetTaxByState(string state)
        {
            switch (state)
            {
                case "Fr":
                    return 20;
                default:
                    return 0;
            }
        }
    }
}
