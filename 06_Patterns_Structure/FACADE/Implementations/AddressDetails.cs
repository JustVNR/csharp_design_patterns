﻿using Patterns_Structure.FACADE.Interfaces;
using Patterns_Structure.FACADE.Models;

namespace Patterns_Structure.FACADE.Implementations
{
    public class AddressDetails : IAddress
    {
        public Address GetAddressDetails(int userID)
        {
            return new Address()
            {
                State = "France"
            };
        }
    }
}
