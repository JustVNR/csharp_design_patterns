﻿using Patterns_Structure.FACADE.Interfaces;

namespace Patterns_Structure.FACADE.Implementations
{
    public class ShoppingCartDetails : ICart
    {
        public int AddItemToCart(int itemID, int quantity)
        {
            return 1;
        }

        public bool CheckItemAvability(Models.Product product)
        {
            return true;
        }

        public double GetCartPrice(int caertID)
        {
            return 1500;
        }

        public Models.Product GetItemDetails(int itemID)
        {
            return new Models.Product() {
                ProductID = 1,
                Name = "default Product",
                Description = "Default Description",
                Quantity = 5,
                Cost = 500,
                LockedQty = 1};
        }

        public bool LockItemInStock(int itemID, int quantity)
        {
            return true;
        }
    }
}
