﻿using Patterns_Structure.FACADE.Models;

namespace Patterns_Structure.FACADE.Interfaces
{
    public interface ICart
    {
        Product GetItemDetails(int itemID);
        bool CheckItemAvability(Product product);
        bool LockItemInStock(int itemID, int quantity);
        int AddItemToCart(int itemID, int quantity);
        double GetCartPrice(int caertID);
    }
}
