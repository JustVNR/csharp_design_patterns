﻿namespace Patterns_Structure.FACADE.Interfaces
{
    public interface IWallet
    {
        double GetUSerBalance(int userID);
    }
}
