﻿using Patterns_Structure.FACADE.Models;

namespace Patterns_Structure.FACADE.Interfaces
{
    public interface IAddress
    {
        Address GetAddressDetails(int userID);
    }
}
