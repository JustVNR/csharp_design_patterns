﻿using Patterns_Structure.FACADE.Implementations;
using Patterns_Structure.FACADE.Interfaces;
using Patterns_Structure.FACADE.Models;
using Product = Patterns_Structure.FACADE.Models.Product;

namespace Patterns_Structure.FACADE.ShoppingFacade
{
    public class UserOrder : IUserOrder
    {
        public int AddToCart(int itemID, int qty)
        {
            Console.WriteLine("Start AddToCart");

            ICart userCart = new ShoppingCartDetails();

            int cartID = 0;

            // Step 1 : GetItem
            Product product = userCart.GetItemDetails(itemID);

            // Step 2 : Check Avability

            if (userCart.CheckItemAvability(product))
            {
                // Step 3 : Lock Item in the Stock
                userCart.LockItemInStock(itemID, qty);

                // Step 4 : Add Item to cart
                cartID = userCart.AddItemToCart(itemID, qty);
            }
            Console.WriteLine("End AddToCart");

            return cartID;
        }

        public int PlaceOrder(int cartID, int userID)
        {
            Console.WriteLine("Start PlaceOrder");

            int orderID = -1;

            IWallet wallet = new Wallet();

            ITax tax = new Tax();

            ICart userCart = new ShoppingCartDetails();

            IAddress address = new AddressDetails();

            IOrder order = new Order();

            // Step 1 : Get tax percenttage state
            double stateTax = tax.GetTaxByState("FR");

            // Step 2 : Apply Tax on the Cart Items
            tax.ApplyTax(cartID, stateTax);

            // Step 3 : Get user WAllet balance
            double userWalletBalance = wallet.GetUSerBalance(userID);

            // Step 4 : Get the cart Item price
            double cartPrice = userCart.GetCartPrice(cartID);

            // Step 5 : Compare the balance abd the price
            if (userWalletBalance > cartPrice)
            {
                // Step 6 : Get User Address and set to cart
                Address userAddress = address.GetAddressDetails(userID);

                // Step 7 : Place Order
                orderID = order.PlaceOrderDetails(cartID, userAddress.AddressID);
            }
             
            Console.WriteLine("End PlaceOrder");

            return orderID;
        }
    }
}
