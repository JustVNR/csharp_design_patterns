﻿namespace Patterns_Structure.Composite
{
    public class Product : Box
    {
        public Product(string description, double price)
        {
            Description = description;
            Price = price;
        }

        public string Description { get; set; } = String.Empty;

        public double Price { get; set; }


        public double GetPrice()
        {
            return Price;
        }
    }
}
