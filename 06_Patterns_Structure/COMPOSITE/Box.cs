﻿namespace Patterns_Structure.Composite
{
    public interface Box
    {

        double GetPrice();
    }
}
