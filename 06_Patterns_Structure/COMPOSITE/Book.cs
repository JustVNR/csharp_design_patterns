﻿namespace Patterns_Structure.Composite
{
    public class Book : Product
    {
        public Book(string description, double price) : base(description, price)
        {
        }
    }
}
