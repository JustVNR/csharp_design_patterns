﻿using System.Linq;

namespace Patterns_Structure.Composite
{


    public class CompositeBox : Box
    {
        public List<Box> Children { get; private set; } = new();

        public CompositeBox(params Box[] children)
        {
            Children.AddRange(children);
        }

        public double GetPrice()
        {
            double result = 0;

            Children.ForEach(b => { result += b.GetPrice(); });
            //Children.AsQueryable().Sum(b => b.GetPrice());

            return result;
        }
    }
}
