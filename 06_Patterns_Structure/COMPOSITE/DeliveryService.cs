﻿namespace Patterns_Structure.Composite
{
    public class DeliveryService
    {
        private Box? box;

        public DeliveryService()
        {

        }

        public void SetupOrder(params Box[] boxes)
        {
            box = new CompositeBox(boxes);
        }

        public double CalculateOrderPrice()
        {
            if (box is null) { return 0; }

            return box.GetPrice();
        }
    }
}
