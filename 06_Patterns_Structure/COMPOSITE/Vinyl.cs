﻿namespace Patterns_Structure.Composite
{
    public class Vinyl : Product
    {
        public Vinyl(string description, double price) : base(description, price)
        {
        }
    }
}
