﻿namespace Patterns_Structure.ADAPTER
{
    public interface IStudent
    {
        string GetName();
        string GetSurname();
        string GetEmail();

        string ToString();
    }
}
