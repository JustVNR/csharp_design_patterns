﻿using System.Xml.Linq;

namespace Patterns_Structure.ADAPTER
{
    public class SchoolStudentAdapter : IStudent
    {
        public SchoolStudent ShoolStudent { get; set; }

        public SchoolStudentAdapter(SchoolStudent shoolStudent)
        {
            ShoolStudent = shoolStudent;
        }

        public string GetEmail()
        {
            return ShoolStudent.GetEmailAddress();
        }

        public string GetName()
        {
            return ShoolStudent.GetFirstName();
        }

        public string GetSurname()
        {
            return ShoolStudent.GetLastName();
        }

        public override string ToString()
        {
            return $"{ShoolStudent.GetLastName()} {ShoolStudent.GetFirstName()} {ShoolStudent.GetEmailAddress()}";
        }
    }
}
