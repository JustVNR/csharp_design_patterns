﻿namespace Patterns_Structure.ADAPTER
{
    public class CollegeStudent : IStudent
    {
        public string Name { get; set; } = String.Empty;
        public string SurName { get; set; } = String.Empty;
        public string Email { get; set; } = String.Empty;

        public string GetEmail()
        {
            return Email;
        }

        public string GetName()
        {
            return Name;
        }

        public string GetSurname()
        {
            return SurName;
        }

        public override string ToString()
        {
            return $"{Name} {SurName} { Email}";
        }
    }
}
