﻿namespace Patterns_Structure.ADAPTER
{
    public class SchoolStudent
    {
        public string FirstName { get; set; } = String.Empty;
        public string LastName { get; set; } = String.Empty;
        public string EmailAddress { get; set; } = String.Empty;

        public string GetEmailAddress()
        {
            return EmailAddress;
        }

        public string GetFirstName()
        {
            return FirstName;
        }

        public string GetLastName()
        {
            return LastName;
        }
    }
}
