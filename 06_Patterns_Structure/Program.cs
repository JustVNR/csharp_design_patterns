﻿#region COMPOSITE

/* Composite est un patron de conception structurel qui permet d'agencer les objets dans des arborescences
 * afin de pouvoir traiter ces arborescences comme des objets individuels.
 * Le pattern composite vise à permettre de traiter de la même manière :
 * - des objets individuels
 * - et des compositions d'objets (composites).
 * 
 * Il peut être vu comme une structure arborescente composée de types qui héritent d'un type de base,
 * et il peut représenter une seule partie ou toute une hiérarchie d'objets.
 * 
 * Le modèle peut se décomposer comme suit :
 * - component : interface de base pour tous les objets de la composition.
 * Il doit s'agir soit d'une interface, soit d'une classe abstraite avec les méthodes courantes pour gérer les composites enfants.
 * - leaf y : implémente le comportement par défaut du composant de base. Il ne contient pas de référence aux autres objets.
 * - composite : implémente les méthodes des composants de base et définit les opérations liées aux enfants.
 * - client : peut accéder aux éléments de la composition en utilisant l'objet component de base 
 * 
 * https://youtu.be/oo9AsGqnisk
 */


using Patterns_Structure.ADAPTER;
using Patterns_Structure.Composite;
using Patterns_Structure.FACADE.ShoppingFacade;
using Patterns_Structure.PROXY;

DeliveryService delivery = new();

delivery.SetupOrder(new Book("Harry", 10), new Vinyl("Daft", 25));

var total = delivery.CalculateOrderPrice();

Console.WriteLine(total);
#endregion

#region PROXY

/* Un proxy est une classe se substituant à une autre classe.
 * Par convention et simplicité, le proxy implémente la même interface que la classe à laquelle il se substitue.
 * L'utilisation de ce proxy ajoute une indirection à l'utilisation de la classe à substituer.
 * Un proxy est utilisé principalement pour contrôler l'accès aux méthodes de la classe substituée.
 * Il est également utilisé pour simplifier l'utilisation d'un objet « complexe » à la base :
 * par exemple, si l'objet doit être manipulé à distance (via un réseau) ou si l'objet est consommateur de temps.
 * 
 * https://youtu.be/TS5i-uPXLs8
 */

IInternet proxy = new Proxy("illegal.com");

proxy.ConnectTo("google.com");
proxy.ConnectTo("illegal.com");

#endregion

#region ADAPTER
/* L’Adaptateur est un patron de conception structurel qui permet à des objets incompatibles de collaborer.
 * L’adaptateur fait office d’emballeur (wrapper) entre les 2 objets.
 * Il récupère les appels à un objet et les met dans un format et une interface reconnaissables par le second.
 * Identification : L’adaptateur peut être identifié grâce à son constructeur qui prend une instance
 * d’un type abstrait différent ou d’une interface différente. Lorsque l’une des méthodes de l’adaptateur est appelée,
 * il traduit les paramètres dans un format approprié et redirige l’appel vers une ou plusieurs méthodes de l’objet emballé.
 * 
 * https://www.youtube.com/watch?v=wA3keqCeKtM
 * https://youtu.be/eR22JuwTa54
 */

List<IStudent> students = new ();

CollegeStudent collegeStudent = new() { Name ="Duck", SurName = "Riri", Email = "ririduck@gmail.com" };
SchoolStudent schoolStudent = new() { LastName ="Duck", FirstName = "Fifi", EmailAddress = "fifiduck@gmail.com" };

students.Add(collegeStudent);

// students.Add(schoolStudent); // Erreur SchoolStudent n'implémente pas IStudent

students.Add(new SchoolStudentAdapter(schoolStudent));

students.ForEach(Console.WriteLine);

#endregion

#region FACADE
/* Façade est un patron de conception structurel qui procure une interface offrant un accès simplifié à une librairie,
 * un framework ou à n’importe quel ensemble complexe de classes.
 * Une façade est une classe qui procure une interface simple vers un sous-système complexe de parties mobiles.
 * Les fonctionnalités proposées par la façade seront plus limitées que si vous interagissiez directement avec le sous-système,
 * mais vous pouvez vous contenter de n’inclure que les fonctionnalités qui intéressent votre client.
 * Une façade se révèle très pratique si votre application n’a besoin que d’une partie des fonctionnalités
 * d’une librairie sophistiquée parmi les nombreuses qu’elle propose.
 * Par exemple, une application qui envoie des petites vidéos de chats comiques sur des réseaux sociaux
 * peut potentiellement utiliser une librairie de conversion vidéo professionnelle.
 * Mais la seule chose dont vous ayez réellement besoin, c’est d’une classe dotée d’une méthode encoder(fichier, format).
 * Après avoir créé cette classe et intégré la librairie de conversion vidéo, votre façade est opérationnelle.
 */

IUserOrder userOrder = new UserOrder();
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("\n**************** Facade start ******************\n");

int cartID = userOrder.AddToCart(10, 1);
int userID = 1;
int orderID = userOrder.PlaceOrder(cartID, userID);

Console.WriteLine("\n**************** End start ******************\n");

#endregion