﻿namespace Patterns_Comportement.OBSERVER
{
    public class Product : ISubject<double>
    {
        public string Description { get; set; } = String.Empty;

        public double Prix { get; set; }

        public List<IObserve<double>> Observers { get; set; }


        public Product(string description, double prix)
        {
            Description = description;
            Prix = prix;
            Observers = new List<IObserve<double>>();
        }

        public void Register(IObserve<double> obs)
        {
            Observers.Add(obs);
        }

        public void Unregister(IObserve<double> obs)
        {
            Observers.Remove(obs);
        }
        public void NotifyObserver(double solde)
        {
            foreach (IObserve<double> o in Observers)
            {
                o.Update(solde);
            }
        }
    }
}
