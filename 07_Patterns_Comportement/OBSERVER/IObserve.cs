﻿namespace Patterns_Comportement.OBSERVER
{
    public interface IObserve<T>
    {
        void Update(T t);
    }
}
