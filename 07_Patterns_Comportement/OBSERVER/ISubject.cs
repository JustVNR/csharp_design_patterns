﻿namespace Patterns_Comportement.OBSERVER
{
    public interface ISubject<T>
    {
        void Register(IObserve<T> obs);
        void Unregister(IObserve<T> obs);
        void NotifyObserver(T obj); // NotifyObserver() : juste la notif : Aucune info n'est fournie ici
    }
}
