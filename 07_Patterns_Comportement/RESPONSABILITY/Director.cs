﻿namespace Patterns_Comportement.RESPONSABILITY
{
    public class Director : Staff
    {
        public Director(string name, Staff successor) : base(name, successor)
        {
        }

        public override void HandleComplaint(Complaint c)
        {
            Console.WriteLine("requête traitée par le directeur....");

            c.State = ComplaintState.CLOSED;
        }
    }
}
