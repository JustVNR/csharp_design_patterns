﻿namespace Patterns_Comportement.RESPONSABILITY
{
    public enum ComplaintState
    {
        OPENED, CLOSED
    }
}
