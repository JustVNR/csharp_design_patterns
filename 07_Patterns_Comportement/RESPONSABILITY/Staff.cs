﻿namespace Patterns_Comportement.RESPONSABILITY
{
    public abstract class Staff
    {
        public string Name { get; set; } = string.Empty;

        public Staff Successor { get; set; }

        public Staff(string name, Staff successor)
        {
            Name = name;
            Successor = successor;
        }

        public abstract void HandleComplaint(Complaint c);
    }
}
