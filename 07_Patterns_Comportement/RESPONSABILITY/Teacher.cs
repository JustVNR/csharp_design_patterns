﻿namespace Patterns_Comportement.RESPONSABILITY
{
    internal class Teacher : Staff
    {
        public Teacher(string name, Staff successor) : base(name, successor)
        {
        }

        public override void HandleComplaint(Complaint c)
        {
            if (c.Type == ComplaintType.PROF)
            {
                Console.WriteLine("Req traitée par le prof....");
                c.State = ComplaintState.CLOSED;
            }
            else if (Successor != null)
            {
                Console.WriteLine("Req transmise au responsable pédago....");
                Successor.HandleComplaint(c);
            }
        }
    }
}
