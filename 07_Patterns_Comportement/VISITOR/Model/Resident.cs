﻿namespace Patterns_Comportement.VISITOR.Model
{
    public class Resident : Client
    {
        public override void Accept(Visitor visitor)
        {
            visitor.VisitResident(this);
        }
    }
}
