﻿namespace Patterns_Comportement.VISITOR.Model
{
    public class Company : Client
    {
        public override void Accept(Visitor visitor)
        {
            visitor.VisitCompany(this);
        }
    }
}
