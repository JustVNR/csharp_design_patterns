﻿namespace Patterns_Comportement.VISITOR.Model
{
    public abstract class Client
    {
        public string Name { get; set; } = String.Empty;
        public string Address { get; set; } = String.Empty;
        public string Number { get; set; } = String.Empty;

        public abstract void Accept(Visitor visitor);
    }
}
