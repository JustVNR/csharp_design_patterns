﻿using Patterns_Comportement.VISITOR.Model;

namespace Patterns_Comportement.VISITOR
{
    public interface Visitor
    {
        public void VisitCompany(Company company)
        {
            Console.WriteLine("Sending mail to Company");
        }

        public void VisitResident(Resident resident)
        {
            Console.WriteLine("Sending mail to Resident");
        }
    }
}
