﻿using Patterns_Comportement.VISITOR.Model;

namespace Patterns_Comportement.VISITOR
{
    public class MessagingVisitor : Visitor
    {
        public void SendMails(List<Client> clients)
        {
            clients.ForEach(client => { client.Accept(this); });
        }
    }
}
