﻿namespace Patterns_Comportement.STRATEGY
{
    public class CreditCard
    {
        public string CardNumber { get; set; } = String.Empty;
        public DateTime ExpirationDate { get; set; }
        public int Cvv { get; set; }

        public double Balance { get; set; }

        public CreditCard()
        {

        }

        public CreditCard(string cardNumber, DateTime expirationDate, int cvv)
        {
            CardNumber = cardNumber;
            ExpirationDate = expirationDate;
            Cvv = cvv;
        }
    }
}
