﻿namespace Patterns_Comportement.STRATEGY
{
    public interface IPaymentStrategy
    {
        void CollectPaymentDetails();
        bool ValidatePayementDetails();
        void Pay(int amount);

    }
}
