﻿namespace Patterns_Comportement.STRATEGY
{
    public class PaymentByCreditCard : IPaymentStrategy
    {
        public CreditCard Card { get; set; } = new();


        public void CollectPaymentDetails()
        {
            // Popup to collect credit card details...
            Card = new("aaa", DateTime.Now, 123);
        }

        public void Pay(int amount)
        {
            Console.WriteLine($"Paying {amount} with credit card");
        }

        public bool ValidatePayementDetails()
        {
            // TODO : Validate code...
            return true;
        }
    }
}
