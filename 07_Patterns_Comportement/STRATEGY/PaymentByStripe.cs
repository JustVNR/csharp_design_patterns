﻿namespace Patterns_Comportement.STRATEGY
{
    public class PaymentByStripe : IPaymentStrategy
    {

        public string Email { get; set; } = String.Empty;
        public string Password { get; set; } = String.Empty;
        public void CollectPaymentDetails()
        {
            // Popup to collect user details...
            Email = "email@toto.com";
            Password = "password";
        }

        public void Pay(int amount)
        {
            Console.WriteLine($"Paying {amount} with Stripe");
        }

        public bool ValidatePayementDetails()
        {
            // TODO : Validate code...
            return true;
        }
    }
}
