﻿namespace Patterns_Comportement.STRATEGY
{
    public class PaymentService
    {

        public int Cost { get; set; }

        public bool IncludeDelivery { get; set; }


        // Pb :
        // - ne respecte pas Open/ Close
        // - ne respecte pas Single Of Responsability
        //public void ProcessOrder(string paymentMethod)
        //{
        //    if (paymentMethod.Equals("CreditCard"))
        //    {

        //        // Popup to collect credit card details...
        //        CreditCard card = new("aaa", DateTime.Now, 123);

        //        Console.WriteLine($"Paying {GetTotal()} with credit card");
        //    }
        //    else if (paymentMethod.Equals("Stripe"))
        //    {
        //        // Popup to collect Stripe details...

        //        Console.WriteLine($"Paying {GetTotal()} with Stripe");
        //    }
        //}

        public IPaymentStrategy Strategy { get; set; }

        public PaymentService(IPaymentStrategy strategy)
        {
            Strategy = strategy;
        }

        public PaymentService(int cost, bool includeDelivery, IPaymentStrategy strategy)
        {
            Cost = cost;
            IncludeDelivery = includeDelivery;
            Strategy = strategy;
        }

        public void ProcessOrder()
        {
            Strategy.CollectPaymentDetails();

            if (Strategy.ValidatePayementDetails())
            {
                Strategy.Pay(GetTotal());
            }
        }

        private int GetTotal()
        {
            return IncludeDelivery ? Cost + 10 : Cost;
        }
    }
}
