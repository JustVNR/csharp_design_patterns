﻿
using Patterns_Comportement.MEMENTO;
using Patterns_Comportement.OBSERVER;
using Patterns_Comportement.RESPONSABILITY;
using Patterns_Comportement.STRATEGY;
using Patterns_Comportement.VISITOR;
using Patterns_Comportement.VISITOR.Model;

#region MEMENTO


Console.WriteLine("\n************************ MEMENTO *********************************\n");

// Comment sauvegarder EN MEMOIRE et rétablir l'état antérieur d'un objet (Undo)
// Exemple éditeur de texte

// Comment sauvegarder et rétablir l'état précédent d'un objet (undo - ctr+z)
// Ex: editeur de texte
/*
 * a ab abc
 * 
 */
Editeur editeur = new();
History history = new();

editeur.Content = "a";
history.Push(editeur.CreateState());
editeur.Content = "ab";
history.Push(editeur.CreateState());
editeur.Content = "abc";
history.Push(editeur.CreateState());

Console.WriteLine("Etat actuel de l'editeur: " + editeur.Content); // abc

editeur.Restore(history.Pop());

Console.WriteLine("1er restore: " + editeur.Content); // ab

editeur.Restore(history.Pop());

Console.WriteLine("2ème restore: " + editeur.Content); // a

editeur.Restore(history.Pop());

Console.WriteLine("3ème restore: " + editeur.Content); //
#endregion

#region OBSERVER
Console.ForegroundColor= ConsoleColor.Green;
Console.WriteLine("\n************************ OBSERVER *************************\n");

Product prod = new("PC DELL", 1200);

IObserve<double> ob1 = new Patterns_Comportement.OBSERVER.Client("Client1");
IObserve<double> ob2 = new Patterns_Comportement.OBSERVER.Client("Client2");

prod.Register(ob1);
prod.Register(ob2);

prod.Prix = 800; // ce changement de prix déclenche 2 notifications

Console.WriteLine("=================================\n");
//Sujet Solde d'un compte bancaire : notifier client si solde négatif

BankAccount c = new("45454D54", 500);

IObserve<double> ob3 = new Customer("Customer 1");

c.Register(ob3);
c.Solde = 1000;
c.Solde = -1;
#endregion

#region Chain Of Responsability
Console.ForegroundColor = ConsoleColor.Yellow;
Console.WriteLine("\n*************** CHAINE OF RESPONSABILITY ***************************\n");

Teacher t = new("Prof", new HeadTeacher("Responsable pédago", new Director("Directeur", null)));

t.HandleComplaint(new Complaint(145, ComplaintType.PROF, "req1", ComplaintState.OPENED));
Console.WriteLine("=================================");

t.HandleComplaint(new Complaint(500, ComplaintType.PEDAGO, "req1", ComplaintState.OPENED));
Console.WriteLine("=================================");

t.HandleComplaint(new Complaint(450, ComplaintType.DIRLO, "req1", ComplaintState.OPENED));
Console.WriteLine("=================================");
#endregion


#region STRATEGY
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("\n*************** STRATEGY ***************************\n");

/*Le design pattern Strategy permet de définir une famille d'algorithmes, de les encapsuler et de les rendre interchangeables.
 * Le principe est de séparer la logique métier (ou le comportement) d'un objet de son implémentation concrète.
 * En utilisant ce patron, une classe peut avoir plusieurs stratégies différentes pour effectuer une tâche spécifique, 
 * et ces stratégies peuvent être échangées à la volée en fonction des besoins du programme.
 * 
 * Le design pattern Strategy permet de :
 * - simplifier le code 
 * - faciliter sa maintenance et l'évolutivité du code
 * 
 * https://youtu.be/Nrwj3gZiuJU
 */

PaymentService ps = new(10, true, new PaymentByCreditCard());

ps.ProcessOrder();

#endregion

#region VISITOR
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("\n*************** VISITOR ***************************\n");


// https://youtu.be/UQP5XqMqtqQ


/*
 * Vise à séparer les traitements des objets auxquels ils s'appliquent.
 * Permet de modifier/ajouter lesdits/de nouveaux traitements sans avoir à modifier le modèle objet.
 * S'applique à des hiérachies d'objets
 * 
 Comment? 

 1/ Créer une interface Visitor qui référence l'ensemble des interfaces de la hierarchies d'objets
 2/ Ajouter un méthode 'Accept(Visitor v)' à chaque objet de la hiérarchie

https://youtu.be/wzfe7DbzgtI
 */

MessagingVisitor mv = new();

List<Patterns_Comportement.VISITOR.Model.Client> clients = new()
{
    new Company(),
    new Resident()
};

mv.SendMails(clients);

#endregion

Console.ForegroundColor = ConsoleColor.White;

