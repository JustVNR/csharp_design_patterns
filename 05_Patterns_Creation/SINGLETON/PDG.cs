﻿using System.Xml.Linq;

namespace Patterns_Creation.SINGLETON
{
    public class PDG
    {
        public string Name { get; set; } = String.Empty;

        private static PDG? Instance { get; set; }

        // La présence d'un constructeur privé supprime le constructeur public par défaut.
        // Seul le singleton peut s'instancier lui-même.
        private PDG()
        {

        }

        /**
          * Méthode permettant de renvoyer une instance de la classe Singleton
          * Mot clé synchronized : pour verrouiller l'accès à la méthode en cas de multi thread (pas d'accès parallèle)
          */
        public static PDG GetInstance(string name)
        {
            Instance ??= new PDG();

            Instance.Name = name;

            return Instance;
        }
    }
}
