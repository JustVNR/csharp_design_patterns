﻿using Patterns_Creation.ABSTRACT_FACTORY;
using Patterns_Creation.FACTORY;
using Patterns_Creation.SINGLETON;


#region FACTORY
// https://youtu.be/EdFq_JIThqM
/* Le design pattern Factory est un pattern de création permetant d'encapsuler la logique de création d'objets dans une classe dédiée.
 * L'idée est de déléguer la responsabilité de l'instanciation d'objets à une classe distincte, plutôt que de la gérer directement dans la classe qui utilise ces objets.
 * L'intérêt principal de ce pattern est de favoriser une bonne encapsulation et une meilleure modularité du code.
 * En utilisant une Factory, on peut réduire la dépendance entre les classes et faciliter la maintenance et l'évolutivité du code.
 */


IVehicule maVoiture = VehiculeFactory<Voiture>.Create();
IVehicule monCamion = VehiculeFactory<Camion>.Create();

maVoiture.Accelerer(); // La voiture accélère
monCamion.Accelerer(); // Le camion accélère
#endregion

#region ABSTRACT FACTORY

/* La fabrique abstraite est un patron de conception créationnel utilisé en génie logiciel orienté objet.
 * Elle fournit une interface pour créer des familles d'objets liés ou inter-dépendants
 * sans avoir à préciser au moment de leur création la classe concrète à utiliser.
 * 
 * Quand utiliser le modèle fabrique abstraite :
 * - Le client est indépendant de la façon dont sont créés et composés les objets
 * - Le système se compose de plusieurs familles d'objets conçues pour être utilisées ensemble
 * - Nous avons besoin d'une valeur d'exécution pour construire une dépendance particulière
 * 
 * Bien que le modèle soit idéal lors de la création d'objets prédéfinis, l'ajout de nouveaux objets peut être difficile.
 * Pour prendre en charge le nouveau type d'objets, il faudra modifier la classe AbstractFactory et toutes ses sous-classes.
 */

Company msi = new MsiManufacturer();
Gpu msiGPU = msi.CreateGpu();
Patterns_Creation.ABSTRACT_FACTORY.Monitor msiMonitor = msi.CreateMonitor();

Company asus = new AsusManufacturer();
Gpu asusGPU = asus.CreateGpu();
Patterns_Creation.ABSTRACT_FACTORY.Monitor asusMonitor = asus.CreateMonitor();

msiGPU.Assemble();
asusMonitor.Assemble();

#endregion

#region SINGLETON
/*En génie logiciel, le singleton est un patron de conception (design pattern)
 * dont l'objectif est de restreindre l'instanciation d'une classe à un seul objet (ou bien à quelques objets seulement).
 * 
 * Il est utilisé lorsqu'on a besoin exactement d'un objet pour coordonner des opérations dans un système.
 */
// https://youtu.be/tSZn4wkBIu8

PDG pdg1 = PDG.GetInstance("pdg1");
PDG pdg2 = PDG.GetInstance("pdg2");

Console.WriteLine(pdg1.Name);
Console.WriteLine(pdg2.Name);
#endregion