﻿namespace Patterns_Creation.ABSTRACT_FACTORY
{
    public class AsusGpu : Gpu
    {
        public void Assemble()
        {
            Console.WriteLine("ASUS GPU assemebled");
        }
    }
}
