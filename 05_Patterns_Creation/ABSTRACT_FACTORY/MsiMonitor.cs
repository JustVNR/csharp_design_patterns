﻿namespace Patterns_Creation.ABSTRACT_FACTORY
{
    public class MsiMonitor : Monitor
    {
        public void Assemble()
        {
            Console.WriteLine("%SI Monitor assemebled");
        }
    }
}
