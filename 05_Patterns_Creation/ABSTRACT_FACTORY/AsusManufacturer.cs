﻿namespace Patterns_Creation.ABSTRACT_FACTORY
{
    public class AsusManufacturer : Company
    {
        public override Gpu CreateGpu()
        {
            return new AsusGpu();
        }

        public override Monitor CreateMonitor()
        {
            return new AsusMonitor();
        }
    }
}
