﻿namespace Patterns_Creation.ABSTRACT_FACTORY
{
    public interface Monitor
    {
        void Assemble();
    }
}
