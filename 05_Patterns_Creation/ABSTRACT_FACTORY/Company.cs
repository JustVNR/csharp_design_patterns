﻿namespace Patterns_Creation.ABSTRACT_FACTORY
{
    public abstract class Company
    {
        public abstract Gpu CreateGpu();
        public abstract Monitor CreateMonitor();
    }
}
