﻿namespace Patterns_Creation.ABSTRACT_FACTORY
{
    public interface Gpu
    {
        void Assemble();
    }
}
