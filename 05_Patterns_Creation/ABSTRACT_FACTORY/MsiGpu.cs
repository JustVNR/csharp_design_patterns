﻿namespace Patterns_Creation.ABSTRACT_FACTORY
{
    public class MsiGpu : Gpu
    {
        public void Assemble()
        {
            Console.WriteLine("MSI GPU assembled");
        }
    }
}
