﻿namespace Patterns_Creation.ABSTRACT_FACTORY
{
    public class AsusMonitor : Monitor
    {
        public void Assemble()
        {
            Console.WriteLine("Asus Monitor assemebled");
        }
    }
}
