﻿namespace Patterns_Creation.FACTORY
{
    public static class VehiculeFactory<T> where T : IVehicule, new()
    {
        //public static IVehicule Create(string type)
        //{
        //    switch (type)
        //    {
        //        case "Voiture":
        //            return new Voiture();
        //        case "Camion":
        //            return new Camion();
        //        default:
        //            throw new ArgumentException("Type de véhicule inconnu", "type");
        //    }
        //    // Ne respecte pas le principe Open/close car il faut modifier la méthode si on rajoute un type de véhicule
        //}

        public static T Create()
        {
            return new T();
        }
    }
}
