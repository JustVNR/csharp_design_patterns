﻿namespace Patterns_Creation.FACTORY
{
    public class Camion : IVehicule
    {
        public void Accelerer()
        {
            Console.WriteLine("Le camion accélère");
        }
    }
}
