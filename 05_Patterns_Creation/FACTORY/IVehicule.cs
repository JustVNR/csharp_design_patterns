﻿namespace Patterns_Creation.FACTORY
{
    public interface IVehicule
    {
        void Accelerer();
    }
}
