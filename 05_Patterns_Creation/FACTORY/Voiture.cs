﻿namespace Patterns_Creation.FACTORY
{
    public class Voiture : IVehicule
    {
        public void Accelerer()
        {
            Console.WriteLine("La voiture accélère");
        }
    }
}
