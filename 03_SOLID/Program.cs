﻿#region LISKOV
using nok = SOLID.L.NOK;
using SOLID.L.OK;

try
{
    nok.Bird parent = new nok.Eagle();

    parent.Fly();

    parent = new nok.Penguin();

    // parent.Fly(); // un pingouin ne peut pas voler => principe de substitution de Liskov non respecté

    FlyingBird eagle = new();
    eagle.Fly();

    SwimmingBird penguin = new();
    penguin.Swim();
}
catch (Exception e)
{

    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine(e.Message);
    Console.ForegroundColor = ConsoleColor.White;
}

#endregion

#region Interface Segregation Principle 
/*
 * Le principe de ségrégation des interfaces stipule que les interfaces d'une classe doivent être séparées en des interfaces spécifiques.
 * 
 * Plus précisément, le principe ISP affirme qu'une classe ne devrait pas être forcée de mettre en œuvre des méthodes qu'elle n'utilise pas ou dont elle n'a pas besoin.
 * 
 * En d'autres termes, une interface ne doit contenir que les méthodes nécessaires à ses clients, sans inclure de méthodes superflues ou non pertinentes pour ceux-ci.
 */

#endregion