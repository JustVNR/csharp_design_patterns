﻿namespace SOLID.S.OK
{
    public class Employe
    {
        public int Id { get; set; }
        public string Name { get; set; } = String.Empty;
        public string Address { get; set; } = String.Empty;
    }
}
