﻿namespace SOLID.S
{
    public class Employe
    {
        public int Id { get; set; }

        public string Name { get; set; } = String.Empty;
        public string Address { get; set; } = String.Empty;

        //Cette méthode ne respecte pas le single of responsability : ce n'est pas à la classe employe de pouvoir déterminer quels employes ont droit à une promotion
        // A la place on va créer une classe RHPromotion
        public bool DeservesPromotion()
        {
            return false;
        }

        // Ne repecte pas SOR non plus : ce n'est pas du ressort de l'Employe de gérer sa persistance en base
        // A la place on va créer une Classe EmployeDAO
        public void Persist()
        {
        }
    }
}

