﻿namespace SOLID.L.NOK
{
    public class Penguin : Bird
    {
        public override void Fly()
        {
            throw new Exception("A Penguin can not fly...");
        }
    }
}
