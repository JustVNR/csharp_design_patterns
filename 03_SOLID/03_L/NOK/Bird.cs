﻿namespace SOLID.L.NOK
{
    /*Le principe de substitution de Liskov stipule que les objets d'une classe fille doivent être substituables à ceux de leur classe de mère sans altérer la cohérence du programme.
     * Une classe mère utilisée dans le code devra donc pouvoir être remplaçée par une instance d'une de ses classes filles sans causer d'erreurs ou d'effets secondaires inattendus.*/
    public abstract class Bird
    {
        public virtual void Fly()
        {
            Console.WriteLine("I Believe I can Fly...");
        }
    }
}
