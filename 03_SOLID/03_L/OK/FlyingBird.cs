﻿namespace SOLID.L.OK
{
    public class FlyingBird
    {
        public virtual void Fly()
        {
            Console.WriteLine("I Believe I can Fly...");
        }
    }
}
