﻿namespace SOLID.L.OK
{
    public class SwimmingBird
    {
        public virtual void Swim()
        {
            Console.WriteLine("I Believe I can Swim...");
        }
    }
}
