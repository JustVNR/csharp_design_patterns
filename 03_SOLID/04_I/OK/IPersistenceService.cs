﻿namespace SOLID.I.OK
{
    public interface IPersistenceService
    {
        List<User> GetAll();
        User GetById(int id);
        void Delete(User u);
        void Insert(User u);
    }
}
