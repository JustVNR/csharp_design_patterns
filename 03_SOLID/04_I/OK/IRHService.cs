﻿namespace SOLID._04_I.OK
{
    public interface IRHService
    {
        string GetContratType();
        DateTime GetDateEmbauche();
    }
}
