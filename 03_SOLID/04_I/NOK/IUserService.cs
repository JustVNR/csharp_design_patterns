﻿using SOLID.I.NOK;

namespace SOLID._04_I.NOK
{
    //Problème : l'interface gère à la fois la persitence des utilisateurs et les ressources humaines
    public interface IUserService
    {
        List<User> GetAll();
        User GetById(int id);
        void Delete(User u);
        void Insert(User u);

        String GgetContratType();
        DateTime GetDateEmbauche();
    }
}
/*
 * Il faut splitter l'interface IUserService en 2 interfaces :
 * - l'une consacrée à la persitence,
 * - l'autre aux ressources humaines.
 */