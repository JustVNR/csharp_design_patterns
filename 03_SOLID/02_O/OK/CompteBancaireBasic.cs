﻿namespace SOLID.O.OK
{
    public class CompteBancaireBasic : CompteBancaire
    {
        public override void Retrait(decimal montant)
        {
            if (montant > Solde)
            {
                throw new Exception("Fonds insuffisants");
            }

            Solde -= montant;
        }
    }
}
