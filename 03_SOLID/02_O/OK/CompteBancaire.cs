﻿namespace SOLID.O.OK
{

    /* Ici CompteBancaire est une classe abstaite avec une méthode abstraite Retrait.
     * 2 sous-classes l'implémentent :
     * - CompteBancaireBasic
     * - CompteBancaireBlocable.
     * Chacune implémentant la méthode Retrait de manière différente.
     * Pour ajouter une nouvelle fonctionnalité il suffit de rajouter une nouvelle classe spécialisée sans avoir à modifier le code existant.*/

    public abstract class CompteBancaire
    {
        public string Titulaire { get; set; } = String.Empty;
        public decimal Solde { get; set; }

        public abstract void Retrait(decimal montant);
    }
}
