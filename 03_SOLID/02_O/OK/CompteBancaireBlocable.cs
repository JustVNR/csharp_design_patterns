﻿namespace SOLID.O.OK
{
    public class CompteBancaireBlocable : CompteBancaire
    {
        private int _retraitsInfructueux = 0;

        public override void Retrait(decimal montant)
        {
            if (_retraitsInfructueux >= 3)
            {
                throw new Exception("Compte bloqué");
            }

            if (montant > Solde)
            {
                _retraitsInfructueux++;
                throw new Exception("Fonds insuffisants");
            }

            Solde -= montant;
        }
    }
}
