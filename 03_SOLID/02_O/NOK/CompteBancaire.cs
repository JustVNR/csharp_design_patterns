﻿namespace SOLID.O.NOK
{
    /* Le principe Open/Closed (ou Ouvert/Fermé) stipule qu'une classe ou un module doit être ouvert à l'extension mais fermé à la modification.
     * Il doit donc être possible d'ajouter de nouvelles fonctionnalités sans modifier le code existant.
     */

    /*
     * La classe compte bancaire ne respecte pas le principe Open/Close car l'ajout d'une nouvelle fonctionalité (blocage de compte, droit à découvert...)
     * implique la modificaiotn de la classe.
     */

    public class CompteBancaire
    {
        public string Titulaire { get; set; } = String.Empty;
        public decimal Solde { get; set; }

        public void Retrait(decimal montant)
        {
            if (montant > Solde)
            {
                throw new Exception("Fonds insuffisants");
            }

            Solde -= montant;
        }
    }
}
    