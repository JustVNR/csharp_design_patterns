﻿using SOLID.D.OK;

namespace SOLID._05_D.OK
{
    public class ContactService
    {
        // Mettre en évidence la dépendance

        private IContactDAO _dao;

        /*
         * Option1 : injection de la dépendance via le constructeur.
         * Avantage : Un objet de type ContactService est créé dans un état stable
         * Inconvénient: impossible de changer de dépendance entre chaque appel de méthode
         */

        public ContactService(IContactDAO dao)
        {
            _dao = dao;
        }

        public Object? computeContact(int id)
        {
            Contact c = _dao.FindById(id);

            // appliquer un traitement au contact
            return null;
        }

        /*
         * Option2 : injection de la dépendance via les paramètres de la méthode
         * Avantage : possibilité de modifier la dépendance en utilisant le même objet service
         * Inconvénient : dépendance à fournir à chaque appel de la méthode
         */

        public ContactService()
        {

        }

        public Object? ComputeContact(int id, IContactDAO dao)
        {
            Contact c = dao.FindById(id);
            // appliquer un traitement au contact
            return null;
        }

        /*
         * Option3 : injection via le setteur
         * Avantage : dépendance modifiable à tout moment
         * Inconvénient : il faut s'assurer que la dépendance à bien été injectée => à utiliser uniquement si on est contraint
         */

        public IContactDAO _dao2 { get; set; }
    }
}
