﻿namespace SOLID.D.OK
{
    public interface IContactDAO
    {
        Contact FindById(int id);
    }
}
