﻿namespace SOLID.D.NOK
{
    //Pb : Code sans injection de dependence - Méthode complètement dépendente de la couche DAO
    public class ContactService
    {
        // Méthode qui applique un traitement sur un objet de type Contact
        public Object? computeContact(int id)
        {
            ContactDAO repo = new ContactDAO();

            Contact? c = repo.FindById(id);

            // TOODO Appliquer un traitement au contact

            return null;
        }
    }
}
