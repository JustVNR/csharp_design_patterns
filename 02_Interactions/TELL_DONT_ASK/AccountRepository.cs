﻿namespace Interactions.TELL_DONT_ASK
{
    public class AccountRepository
    {
        public Account GetById(int id)
        {
            return new Account(id, 1000);
        }

        public void Save(Account c)
        {
            Console.WriteLine($"Accound {c} saved !");
        }
    }
}
