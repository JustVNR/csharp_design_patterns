﻿namespace Interactions.TELL_DONT_ASK
{
    public class AccountService
    {
        private AccountRepository _repo;

        public AccountService(AccountRepository repo)
        {
            _repo = repo;
        }

        public void WithdrawAsk(int id, double amount)
        {
            Account acc = _repo.GetById(id);

            if (acc.Balance < amount) //ASK : NOT GOOD !!!!
            { 
                throw new ArgumentException("Error: not enough money!");
            }
            acc.Balance -= amount;

            _repo.Save(acc);
        }

        public void WithdrawDontAsk(int id, double amount)
        {
            Account acc = _repo.GetById(id);

            try
            {
                acc.WithDraw(amount);

                _repo.Save(acc);

            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
