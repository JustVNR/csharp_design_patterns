﻿namespace Interactions.TELL_DONT_ASK
{
    public class Account
    {
        public int Id { get; set; }

        public double Balance { get; set; }

        public Account(int id, double balance)
        {
            Id = id;
            Balance = balance;
        }

        public void WithDraw(double amount)
        {
            if (Balance < amount)
            {
                throw new ArgumentException("Solde insuffisant");
            }

            Balance -= amount;
        }

        public override string ToString()
        {
            return $"Id : {Id}, Balance : {Balance}";
        }
    }
}
