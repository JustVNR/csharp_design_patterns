﻿namespace Interactions.DEPENDANCES.HERITAGE
{
    //Pb de l'héritage : tout changement dans la classe mère aura une incidence sur les classes filles

    public class A
    {
        public int Property { get; set; }

        public A(int property)
        {
            Property = property;
        }
    }
}
