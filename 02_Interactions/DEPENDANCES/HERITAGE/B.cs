﻿namespace Interactions.DEPENDANCES.HERITAGE
{
    //Pb de l'héritage : tout changement dans la classe mère aura une incidence sur les classes filles

    public class B : A
    {
        public B(int property) : base(property)
        {
        }
    }
}
