﻿namespace Interactions.DEPENDANCES.RELATION
{

    /* Dépendance de type Relation :
     * La plus faible forme de dépendance entre objets
     * Limitée dans le temps => réduite à  l'exécution de la méthodeB
     * B utilise de manière temporaire l'objet A
     */

    public class A
    {
        public void MethodeA()
        {
            Console.WriteLine("Appel de 'methodeA'");
        }
    }
}
