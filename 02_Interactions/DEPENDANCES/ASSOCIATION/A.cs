﻿namespace Interactions.DEPENDANCES.ASSOCIATION
{
    /*Une dépendance de type Association s'étend sur toute la durée de vie d'un objet.
     * - influence la construction d'objet
     * - l'ensemble des méthodes est partagé
     */
    public class A
    {
        public B B { get; set; }

        public A(B b)
        {
            B = b;
        }
    }
}
