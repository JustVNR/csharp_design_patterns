﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interactions.DEPENDANCES.ASSOCIATION
{
    public class B
    {
        public void Methode()
        {
            Console.WriteLine("Appel de la méthode dans la classe B");
        }
    }
}
