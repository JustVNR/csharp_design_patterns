﻿namespace Interactions.DEMETER
{
    public class StudentClass
    {
        public List<Student> Students { get; set; } = new();

        public int CountStudents()
        {
            return Students.Count;
        }
    }
}
