﻿namespace Interactions.DEMETER
{
    public class School
    {
        private readonly List<StudentClass> Classes = new();
        // une méthode visant à calculer le nombre total de Student

        // Cette méthode ne respecte pas la loi Demeter car faisant appel à des méthodes de classes petites filles
        public int CountStudentsNoDemeter()
        {
            int count = 0;
            foreach (StudentClass classe in Classes)
            {
                foreach (Student s in classe.Students)
                {
                    count++;
                }
            }

            return count;
        }

        public int CountStudentsDemeter()
        {
            int count = 0;
            foreach (StudentClass classe in Classes)
            {
                count += classe.CountStudents();
            }
            return count;
        }
    }
}
