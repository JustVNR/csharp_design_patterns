﻿namespace Interactions.DEMETER
{
    /* La loi de Déméter vise à diminuer le couplage entre objets.
     * Elle suppose qu'une méthode de classe ne devrait faire appel qu'aux éléments de la classe elle-même :
     * - ses paramètres
     * - ses variables locales
     * - ses attributs
     */
    public class Student
    {

    }
}
