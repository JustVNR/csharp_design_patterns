﻿using System;

namespace Interactions.COMPORTEMENTS.OBSERVER
{
    public class BankAccount : ISubject<double>
    {
        public string Numero { get; set; } = String.Empty;

        public double Solde { get; set; }

        public List<IObserve<double>> Observers { get; set; }

        public BankAccount(string numero, double solde)
        {
            Numero = numero;
            Solde = solde;
            Observers = new List<IObserve<double>>();
        }

        public void Register(IObserve<double> obs)
        {
            Observers.Add(obs);
        }

        public void Unregister(IObserve<double> obs)
        {
            Observers.Remove(obs);
        }

        public void NotifyObserver(double solde)
        {
            foreach (IObserve<double> o in Observers)
            {
                o.Update(solde);
            }
        }
    }
}
