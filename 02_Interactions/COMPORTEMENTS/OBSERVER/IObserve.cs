﻿namespace Interactions.COMPORTEMENTS.OBSERVER
{
    public interface IObserve<T>
    {
        void Update(T t);
    }
}
