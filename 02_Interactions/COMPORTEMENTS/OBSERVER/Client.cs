﻿namespace Interactions.COMPORTEMENTS.OBSERVER
{
    public class Client : IObserve<double>
    {
        public string Name { get; set; } = String.Empty;

        public double LastObservedPrice { get; set; }

        public Client(string name)
        {
            Name = name;
        }

        public void Update(double price)
        {
            Console.WriteLine($"{Name} : notification de prix reçue: {price}");
            LastObservedPrice = price;
        }
    }
}
