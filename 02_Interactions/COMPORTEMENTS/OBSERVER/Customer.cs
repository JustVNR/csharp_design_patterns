﻿namespace Interactions.COMPORTEMENTS.OBSERVER
{
    public class Customer : IObserve<double>
    {
        public string Name { get; set; } = String.Empty;

        public double LastObservedSolde { get; set; }

        public Customer(string name)
        {
            Name = name;
        }

        public void Update(double solde)
        {
            if (solde < 0)
            {
                Console.WriteLine($"Notification de solde négatif : {solde}€");
            }

            LastObservedSolde = solde;
        }
    }
}
