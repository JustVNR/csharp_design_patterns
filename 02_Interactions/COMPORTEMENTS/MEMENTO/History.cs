﻿namespace Interactions.COMPORTEMENTS.MEMENTO
{
    public class History
    {
        private readonly List<EditeurState> states;

        public History()
        {
            states = new List<EditeurState>();
        }

        public void Push(EditeurState state)
        {
            states.Add(state);
        }

        public EditeurState? Pop()
        {

            EditeurState? last = states.LastOrDefault();

            if(last is not null) states.Remove(last);

            return last;
        }
    }
}
