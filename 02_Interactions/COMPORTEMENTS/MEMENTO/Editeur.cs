﻿namespace Interactions.COMPORTEMENTS.MEMENTO
{
    public class Editeur
    {
        public String Content { get; set; } = String.Empty;

        public EditeurState CreateState()
        {
            return new EditeurState(Content);
        }

        public void Restore(EditeurState? state)
        {
            Content = state == null ? String.Empty : state.Content;
        }
    }
}
