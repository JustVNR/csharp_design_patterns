﻿namespace Interactions.COMPORTEMENTS.MEMENTO
{
    public class EditeurState
    {
        public string Content { get; set; } = String.Empty;

        public EditeurState(string content)
        {
            Content = content;
        }
    }
}
