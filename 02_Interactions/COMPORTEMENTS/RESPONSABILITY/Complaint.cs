﻿namespace Interactions.COMPORTEMENTS.RESPONSABILITY
{
    public class Complaint
    {
        public int StudentId { get; set; }

        public ComplaintType Type { get; set; }
        //1 traitée par le prof
        //2 : traitée par leresponsable pédagogoque
        //3 : traitée par ledirecteur
        public ComplaintState State { get; set; }

        public string Message { get; set; } = string.Empty;

        public Complaint(int studentId, ComplaintType type, string message, ComplaintState state)
        {
            StudentId = studentId;
            Type = type;
            Message = message;
            State = state;
        }
    }
}
