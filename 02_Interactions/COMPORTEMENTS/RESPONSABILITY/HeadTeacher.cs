﻿namespace Interactions.COMPORTEMENTS.RESPONSABILITY
{
    public class HeadTeacher : Staff
    {
        public HeadTeacher(string name, Staff successor) : base(name, successor)
        {
        }

        public override void HandleComplaint(Complaint c)
        {
            if (c.Type == ComplaintType.PEDAGO)
            {
                Console.WriteLine("Req traitée par le responsable pédagogique....");
                c.State = ComplaintState.CLOSED;
            }
            else if (Successor != null)
            {
                Console.WriteLine("Req transmise au directeur....");
                Successor.HandleComplaint(c);
            }
        }
    }
}
