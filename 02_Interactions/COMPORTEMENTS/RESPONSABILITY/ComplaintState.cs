﻿namespace Interactions.COMPORTEMENTS.RESPONSABILITY
{
    public enum ComplaintState
    {
        OPENED, CLOSED
    }
}
