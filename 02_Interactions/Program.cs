﻿using Interactions.TELL_DONT_ASK;
using Asso = Interactions.DEPENDANCES.ASSOCIATION;
using Herit = Interactions.DEPENDANCES.HERITAGE;
using Rel = Interactions.DEPENDANCES.RELATION;

#region TELL DON'T ASK

Console.WriteLine("********** TELL DON'T ASK ***********\n");
/* Tell dont ask: dites, ne posez pas de questions.
 * Dites à vos objets ce qu'ils doivent faire, ne leur posez pas de questions sur leur Etat.
 */


try
{
    Account account = new(1, 2000);
    AccountService _service = new(new AccountRepository());

    _service.WithdrawAsk(1, 500);
    _service.WithdrawDontAsk(1, 2500);

}
catch (Exception e)
{
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine(e.Message);
    Console.ForegroundColor = ConsoleColor.White;
}
#endregion

#region DEPENDANCE

#region ASSOCIATION


/*Une dépendance de type Association s'étend sur toute la durée de vie d'un objet.
* - influence la construction d'objet
* - l'ensemble des méthodes est partagé
*/

Asso.A aAsso = new(new Asso.B());
aAsso.B.Methode();

#endregion


#region HERITAGE
//Pb de l'héritage : tout changement dans la classe mère aura une incidence sur les classes filles

Herit.A aHerit = new Herit.B(12);

#endregion


#region RELATION
/* Dépendance de type Relation :
* La plus faible forme de dépendance entre objets
* Limitée dans le temps => réduite à  l'exécution de la méthodeB
* B utilise de manière temporaire l'objet A
*/

Rel.B bRel = new();

bRel.MethodeB(new Rel.A());

#endregion

#endregion

#region DEMETER
Console.WriteLine("\n*************** DEMETER ***************************\n");

/* La loi de Déméter vise à diminuer le couplage entre objets.
 * Elle suppose qu'une méthode de classe ne devrait faire appel qu'aux éléments de la classe elle-même :
 * - ses paramètres
 * - ses variables locales
 * - ses attributs
 */

#endregion