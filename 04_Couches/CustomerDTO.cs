﻿namespace Couches
{
    public class CustomerDTO
    {
        public Identity Identity { get; set; } = new();

        public string AccountNumber { get; set; } = String.Empty;

        public override string ToString()
        {
            return $"CustomerDTO [{Identity.FirstName} {Identity.LastName}, Account Number = {AccountNumber}]";
        }
    }
}
