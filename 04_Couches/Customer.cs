﻿namespace Couches
{
    public class Customer
    {
        public Identity Identity { get; set; } = new();

        public Account Account { get; set; } = new();

        // Indispensable pour automapper
        public Customer()
        {

        }
        public Customer(Identity identity, Account account)
        {
            Identity = identity;
            Account = account;
        }

        public override string ToString()
        {
            return $"Customer [{Identity}, {Account}]";
        }
    }
}
