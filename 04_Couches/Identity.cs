﻿namespace Couches
{
    public  class Identity
    {
        public string FirstName { get; set; } = String.Empty;
        public string LastName { get; set; } = String.Empty;


        public Identity()
        {

        }
        public Identity(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public override string ToString()
        {
            return $"Name [firstName = {FirstName}, lastName = {LastName}]";
        }
    }
}
