﻿using Couches;
using Couches.Helpers;

Customer cust = new(new Identity("Riri", "Duck"), new Account("aaaa", 2500F));
Console.WriteLine(cust);

//Model vers DTO

var mapper = MapperConfig.InitializeAutomapper();

CustomerDTO dto = mapper.Map<Customer, CustomerDTO>(cust);
Console.WriteLine(dto);

//DTO vers Model
Customer cust2 = mapper.Map<CustomerDTO, Customer>(dto);

Console.WriteLine(cust2);

