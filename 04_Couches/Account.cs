﻿namespace Couches
{
    public class Account
    {
        public string Number { get; set; } = String.Empty;

        public float Balance { get; set; }

        public Account()
        {

        }
        public Account(string number, float balance)
        {
            Number = number;
            Balance = balance;
        }

        public override string ToString()
        {
            return $"Account [number = {Number}, balance = {Balance}]";
        }
    }
}
