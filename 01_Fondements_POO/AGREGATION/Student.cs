﻿namespace Fondements_POO.AGREGATION
{
    public class Student
    {
        public int Id { get; set; }

        public string Nom { get; set; }

        public Grades Notes { get; set; }

        public Student(int id, string nom, Grades notes)
        {
            Id = id;
            Nom = nom;
            Notes = notes;
        }

        public override string ToString()
        {
            return $"Etudiant [id= Id, nom = {Nom}, notes = {Notes}]";
        }
    }
}
