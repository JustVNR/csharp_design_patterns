﻿namespace Fondements_POO.AGREGATION
{
    public class Grades
    {

        public int Maths { get; set; }
        public int English { get; set; }

        public Grades(int maths, int english)
        {
            Maths = maths;
            English = english;
        }

        public override string ToString()
        {
            return $"Notes [maths = {Maths}, english = {English}]";
        }
    }
}
