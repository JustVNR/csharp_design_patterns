﻿namespace Fondements_POO.HERITAGE
{
    public class Labrador : Chien
    {
        public bool GuideAveugle { get; set; }
        public Labrador(int age, string couleur) : base(age, couleur)
        {
        }

        public Labrador(int age, string couleur, bool guideAveugle) : this(age, couleur)
        {
            GuideAveugle = guideAveugle;
        }

        public override void Identite()
        {
            Console.WriteLine("Je suis un labrador");
        }

        public override string ToString()
        {
            return base.ToString() + $" - GuideAveugle : {GuideAveugle}";
        }
    }
}
