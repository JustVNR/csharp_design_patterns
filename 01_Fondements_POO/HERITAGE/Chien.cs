﻿namespace Fondements_POO.HERITAGE
{
    public class Chien
    {
        public int Age { get; set; }
        public string? Couleur { get; set; }

        public Chien()
        {

        }

        public Chien(int age, string couleur)
        {
            Age = age;
            Couleur = couleur;
        }

        public virtual void Identite()
        {
            Console.WriteLine("Je suis un chien");
        }

        public override string ToString()
        {
            return $"Age : {Age} - Couleur : { Couleur}";
        }
    }
}
