﻿namespace Fondements_POO.OBJET_VALEUR
{

    /*
     * Le concept d'"objet valeur" consiste à transformer des valeurs en objet.
     * 
     * Avantages : 
     * - le code est plus lisible
     * - plus sécurisé
     * 
     * Inconvénients : Alourdit le projet
     */

    public class PersonEntity
    {
        public PId Id { get; set; }
        public PFirstName FirstName { get; set; }
        public PLastName LastName { get; set; }
        public PEmail Email { get; set; }

        public PersonEntity(PId id, PFirstName firstName, PLastName lastName, PEmail email)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public class PId
        {
            public int Value { get; set; }
            public PId(int value)
            {
                Value = value;
            }

            public override string ToString()
            {
                return Value.ToString();
            }
        }

        public class PFirstName
        {
            public string Value { get; set; } = String.Empty;
            public PFirstName(string value)
            {
                Value = value;
            }

            public override string ToString()
            {
                return Value;
            }
        }

        public class PLastName
        {
            public string Value { get; set; } = String.Empty;
            public PLastName(string value)
            {
                Value = value;
            }

            public override string ToString()
            {
                return Value;
            }
        }

        public class PEmail
        {
            public string Value { get; set; } = String.Empty;
            public PEmail(string value)
            {
                Value = value;
            }

            public override string ToString()
            {
                return Value;
            }
        }

        public override string ToString()
        {
            return $"Id = {Id}, FirstName = {FirstName}, LastName = {LastName}, Email = {Email}";
        }
    }
}
