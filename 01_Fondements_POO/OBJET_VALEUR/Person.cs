﻿namespace Fondements_POO.OBJET_VALEUR
{
    // Sans Objet valeur
    public class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; } = String.Empty;
        public string LastName { get; set; } = String.Empty;
        public string Email { get; set; } = String.Empty;

        public Person(int id, string firstName, string lastName, string email)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }
        public override string ToString()
        {
            return $"Id = {Id}, FirstName = {FirstName}, LastName = {LastName}, Email = {Email}";
        }
    }
}
