﻿namespace Fondements_POO.ENCAPSULATION
{
    /* L’encapsulation est un mécanisme consistant à rassembler les attributs et méthodes d'un objet au sein d’une structure cachant son implémentation.
     * Ceci vise à empêcher l’accès aux données par un autre moyen que les services proposés par l'objet.
     * L’encapsulation permet donc de garantir l’intégrité des données contenues dans l’objet.
     * Ainsi, si l’on veut protéger des informations contre une modification inattendue
     * Exemple : longueur négative
     * on doit se référer au principe d’encapsulation.
     * En c# le principe d'encapsulation s'illustre notamment par l'utilisation de méthodes permettant :
     * - d'accéder/lire (Getters)
     * - et de modifier (Setters) les attributs d'une classe.
     */

    public class Rectangle
    {
        public int Largeur { get; set; }

        private int longueur;

        public int Longueur
        {
            get { return longueur; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Une longueur doit être positive");
                }

                longueur = value;
            }
        }

        public Rectangle()
        {

        }
        public Rectangle(int largeur, int longueur)
        {
            Largeur = largeur;
            Longueur = longueur;
        }

        public override string ToString()
        {
            return $"({Largeur} , {Longueur})";
        }
    }
}
