﻿namespace Fondements_POO.POLYMORPHISME
{
    public class Chaise : IPliable
    {
        public void deplier()
        {
            Console.WriteLine("Plier Chaise");
        }

        public void plier()
        {
            Console.WriteLine("Déplier Chaise");
        }
    }
}
