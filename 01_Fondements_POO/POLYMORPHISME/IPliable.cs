﻿namespace Fondements_POO.POLYMORPHISME
{


    //Le polymorphisme (par sous-typage) repose sur l’héritage et la redéfinition (overriding) de méthodes virtuelles.

    public interface IPliable
    {
        void plier();
        void deplier();
    }
}
