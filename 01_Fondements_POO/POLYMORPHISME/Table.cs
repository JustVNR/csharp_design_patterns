﻿namespace Fondements_POO.POLYMORPHISME
{
    public class Table : IPliable
    {
        public void deplier()
        {
            Console.WriteLine("Déplier table");
        }

        public void plier()
        {
            Console.WriteLine("Plier table");
        }
    }
}
