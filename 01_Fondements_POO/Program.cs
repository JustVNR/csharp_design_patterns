﻿
using Fondements_POO.AGREGATION;
using Fondements_POO.ENCAPSULATION;
using Fondements_POO.HERITAGE;
using Fondements_POO.OBJET_VALEUR;
using Fondements_POO.POLYMORPHISME;

#region ENCAPSULATION
Console.ForegroundColor= ConsoleColor.White;
Console.WriteLine("\n**************** ENCAPSULATION ***************\n");
try
{
    Rectangle rect = new();

    rect.Longueur = -3;

    Console.WriteLine(rect);
}
catch (ArgumentException e)
{
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine(e.Message);
    Console.ForegroundColor = ConsoleColor.White;
}
#endregion


#region AGREGATION
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("\n**************** AGREGATION ***************\n");

//L'agrégation permet de définir une entité comme étant liée à une ou plusieurs entités de classes différentes.

Student student = new(1, "Duck", new Grades(10, 12));

Console.WriteLine($"strudent = {student}");
#endregion

#region HERITAGE
Console.ForegroundColor = ConsoleColor.White;
Console.WriteLine("\n**************** HERITAGE ***************\n");
/* L'héritage permet de créer une nouvelle classe à partir d'une classe existante.
 * Le mot "héritage" signifie que la classe fille hérite des attributs et méthodes de sa superclasse.
 * L'intérêt majeur de l'héritage est de pouvoir définir de nouveaux attributs et de nouvelles méthodes pour la classe dérivée,
 * qui viennent s'ajouter à ceux et celles héritées.
 * Ainsi est-il possible de créer une hiérarchie de classes de plus en plus spécialisées sans avoir à repartir de zéro.
 */


Labrador labrador = new(2, "Noir", true);
Console.WriteLine(labrador);


#endregion

#region POLYMORPHISME
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("\n**************** POLYMORPHISME ***************\n");
//Le polymorphisme (par sous-typage) repose sur l’héritage et la redéfinition (overriding) de méthodes virtuelles.

IPliable maChaise = new Chaise();
IPliable maTable = new Table();
maChaise.plier();
maTable.deplier();

// Collection polymorphique
List<IPliable> pliables = new() // Une collection est fortement typée et ne peut donc contenir que des éléments du même type
{
    maChaise, // Mais peut contenir des chaises
    maTable   // Et des tables... 
};

#endregion

#region OBJET VALEUR
Console.ForegroundColor = ConsoleColor.White;
Console.WriteLine("\n**************** OBJET VALEUR ***************\n");

/*
 * Le concept d'"objet valeur" consiste à transformer des valeurs en objet.
 * 
 * Avantages : 
 * - le code est plus lisible
 * - plus sécurisé
 * 
 * Inconvénients : Alourdit le projet
 */

// Instanciation d'un objet de type "Person" sans "objet valeur"
Person p = new(1, "Riri", "Duck", "ririduck@gmail.com");

Person pError = new(1, "ririduck@gmail.com", "Fifi", "Duck"); // Compile malgré l'erreur d'ordonancement des paramètres

Console.WriteLine(pError);
Console.WriteLine();

//Solution : Passer les paramètres sous formes d'objet valeur

PersonEntity pEntity = new(new PersonEntity.PId(1), new PersonEntity.PFirstName("Loulou"), new PersonEntity.PLastName("Duck"), new PersonEntity.PEmail("loulouduck@gmail.com"));
Console.WriteLine(pEntity);

#endregion

#region Cercle vertueux de l'ignorance

/*
 * - On manipule les objets par des méthodes, on ignore donc l'implantation de celles-ci
 * - On manipule les objets à travers leurs interfaces, on ignore donc la classe de l'objets
 * - Cela permet de limiter l'écriture de code spaghetti et renforcer la localité
 */

#endregion